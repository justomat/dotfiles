. ~/.zsh/aliases.zsh
. ~/.zsh/zgen/zgen.zsh
. ~/.nvm/nvm.sh

export PATH="$PATH:$HOME/.cabal/bin"
export PATH="$PATH:$HOME/.composer/vendor/bin"
export PATH="$PATH:$HOME/ellcc/bin"

if ! zgen saved; then
    echo "Creating a zgen save"

    zgen oh-my-zsh
    # load oh-my-zsh plugins
    zgen oh-my-zsh plugins/vagrant
    zgen oh-my-zsh plugins/python
    zgen oh-my-zsh plugins/git
    zgen oh-my-zsh plugins/github
    zgen oh-my-zsh plugins/pip
    zgen oh-my-zsh plugins/sudo
    zgen oh-my-zsh plugins/colored-man
    zgen oh-my-zsh plugins/rsync

    zgen loadall <<EOPLUGINS
        zsh-users/zsh-syntax-highlighting
        srijanshetty/docker-zsh

        # pure theme
        # mafredri/zsh-async
        # sindresorhus/pure
EOPLUGINS

    # ys theme
    zgen oh-my-zsh themes/ys

    # Load more completion files for zsh from the zsh-lovers github repo
    zgen load zsh-users/zsh-completions src

    zgen save
fi

# github/hub
eval "$(hub alias -s)"
