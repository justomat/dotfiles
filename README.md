# Dotfiles

This is my {osx|linux} dotfiles.
It's Managed with [Dotbot](https://github.com/anishathalye/dotbot).

# Install

Clone the repo and its submodules, then install links. The install will not override any configs by default.

    git clone https://github.com/justomat/dotfiles.git && \
    cd dotfiles && \
    git submodule update --init --recursive && \
    ./install.sh
